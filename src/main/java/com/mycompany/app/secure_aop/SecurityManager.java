package com.mycompany.app.secure_aop;

/**
 * Created by marg on 26.05.17.
 */
public class SecurityManager {
    private static ThreadLocal<Userinfo> threadLocal = new ThreadLocal<>();

    public void login(String name, String pass) {
        threadLocal.set(new Userinfo(name, pass));
    }

    public void logout() {
        threadLocal.set(null);
    }

    public void getLoggerUser() {
        threadLocal.get();
    }
}
