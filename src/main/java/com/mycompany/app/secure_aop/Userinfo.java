package com.mycompany.app.secure_aop;

/**
 * Created by marg on 26.05.17.
 */
public class Userinfo {
    private String name;
    private String pass;

    public Userinfo(String name, String pass) {
        this.name = name;
        this.pass = pass;
    }

    public String getName() {
        return name;
    }

    public String getPass() {
        return pass;
    }
}
