package com.mycompany.app;

public class ConstructorConfusion {

    private String someValue;

    public ConstructorConfusion(String someValue) {
        System.out.println("Some value (String)" + someValue);
        this.someValue = someValue;
    }

    public ConstructorConfusion(int someValue) {
        System.out.println("Some value (int)" + someValue);
        this.someValue = Integer.toString(someValue);
    }

    public String getSomeValue() {
        return someValue;
    }
}
