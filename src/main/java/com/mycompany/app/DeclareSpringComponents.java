package com.mycompany.app;

import org.springframework.context.support.GenericXmlApplicationContext;

public class DeclareSpringComponents {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("META-INF/app-context.xml");
        ctx.refresh();
        MessageProvider provider = (MessageProvider) ctx.getBean("messageProvider");

        System.out.println(provider.getMessage());
    }
}
