package com.mycompany.app;

public class ConfigurabeMessageProvider implements MessageProvider {

    private String message;

    public ConfigurabeMessageProvider(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
