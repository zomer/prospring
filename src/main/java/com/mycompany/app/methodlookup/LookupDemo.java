package com.mycompany.app.methodlookup;

import com.mycompany.app.StandardOutMessageRenderer;
import org.springframework.context.support.GenericXmlApplicationContext;

public class LookupDemo {
    public static void main (String [] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("META-INF/methodlookup.xml");
        ctx.refresh();

        StandardOutMessageRenderer messageRenderer = (StandardOutMessageRenderer) ctx.getBean("messageRenderer");
        messageRenderer.render();
    }
}
