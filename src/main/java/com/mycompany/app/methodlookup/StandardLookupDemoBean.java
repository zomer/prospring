package com.mycompany.app.methodlookup;

public class StandardLookupDemoBean implements DemoBean {

    private MyHelper helper;

    public void setHelper(MyHelper helper) {
        this.helper = helper;
    }

    @Override
    public MyHelper getMyHelper() {
        return helper;
    }

    @Override
    public void someOperation() {
        helper.doSomethingHelpful();
    }
}
