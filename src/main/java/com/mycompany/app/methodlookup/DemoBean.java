package com.mycompany.app.methodlookup;

public interface DemoBean {
    MyHelper getMyHelper();
    void someOperation();
}
