package com.mycompany.app;

import org.springframework.context.support.GenericXmlApplicationContext;

public class ConstructorMain {

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx =
                new GenericXmlApplicationContext();
        ctx.load("META-INF/app-context.xml");
        ctx. refresh () ;

        ConstructorConfusion се =
                (ConstructorConfusion) ctx.getBean("constructorConfusion");

        System.out.println(се.getSomeValue());
    }
}
