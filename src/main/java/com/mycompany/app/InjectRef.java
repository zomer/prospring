package com.mycompany.app;

import org.springframework.context.support.GenericXmlApplicationContext;

public class InjectRef {
    private Oracle oracle;

    public void setOracle(Oracle oracle) {
        this.oracle = oracle;
    }

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx =
                new GenericXmlApplicationContext();
        ctx.load("META-INF/app-context.xml");
        ctx. refresh () ;

        InjectRef се =
                (InjectRef) ctx.getBean("injectRef");

        System.out.println(се);
    }

    @Override
    public String toString() {
        return "InjectRef{" +
                "oracle=" + oracle.defineMeaningOfLife() +
                '}';
    }
}
