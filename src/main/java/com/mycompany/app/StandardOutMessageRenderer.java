package com.mycompany.app;

public class StandardOutMessageRenderer implements MessageRender {
    private MessageProvider provider;

    public void render() {
        System.out.println(provider.getMessage());
    }

    public void setMessageProvider(MessageProvider provider) {
        this.provider = provider;
    }

    public MessageProvider getMessageProvider() {
        return provider;
    }
}
