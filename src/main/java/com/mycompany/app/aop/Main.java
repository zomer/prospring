package com.mycompany.app.aop;

import org.springframework.aop.framework.ProxyFactory;

/**
 * Created by marg on 26.05.17.
 */
public class Main {

    public static void main (String[] args) {
        MessageWriter writer = new MessageWriter();

        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addAdvice(new MessageDecorator());
        proxyFactory.setTarget(writer);

        MessageWriter proxy = (MessageWriter) proxyFactory.getProxy();
        writer.writeMessage();
        proxy.writeMessage();
    }
}
