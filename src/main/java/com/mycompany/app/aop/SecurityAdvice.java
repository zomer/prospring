package com.mycompany.app.aop;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * Created by marg on 26.05.17.
 */
public class SecurityAdvice implements MethodBeforeAdvice {

    private SecurityManager manager;

    public SecurityAdvice() {
    }

    @Override
    public void before(Method method, Object[] objects, Object o) throws Throwable {

    }
}
