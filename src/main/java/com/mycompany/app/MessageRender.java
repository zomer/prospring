package com.mycompany.app;

public interface MessageRender {
    void render () ;
    void setMessageProvider(MessageProvider provider);
    MessageProvider getMessageProvider();
}
