package com.mycompany.app.beanfactory;

/**
 * Created by marg on 24.05.17.
 */
public class MessageDigest {
    private MessageDigest degest1;

    private MessageDigest degest2;

    public void setDegest1(MessageDigest degest1) {
        this.degest1 = degest1;
    }

    public void setDegest2(MessageDigest degest2) {
        this.degest2 = degest2;
    }

    public void digest(String msg) {
        System.out.println("using digest 1");
        digest(msg, degest1);

        System.out.println("using digest 2");
        digest(msg, degest2);
    }

    private void digest(String msg, MessageDigest digest) {
//        System. out. println ( "Using alogrithm: " + digest.getAlgorithm ());
//        digest.reset();
//
//        byte [] bytes = msg. getBytes () ;
//        byte [] out = digest.digest(bytes);
//        System.out.println(out);
    }

//    private byte[] digest(byte[] bytes) {
//    }
//
//    private void reset() {
//    }
//
//    private String getAlgorithm() {
//    }
//
//    public static MessageDigest getInstance(String algoritm) {
//    }
}
