package com.mycompany.app.aop_factory;

import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * Created by marg on 26.05.17.
 */
public class ProxyFactoryBeanExample {

    public static void main (String [] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("META-INF/point.xml");
        ctx.refresh();

        MyBean beanl = (MyBean) ctx. getBean ( "myBeanl");
        MyBean bean2 = (MyBean)ctx.getBean("myBean2");

        System.out.println("Bean 1");
        beanl.execute();

        System.out.println("\nBean 2");
        bean2.execute();
    }
}
