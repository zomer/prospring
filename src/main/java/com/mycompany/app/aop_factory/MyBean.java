package com.mycompany.app.aop_factory;

/**
 * Created by marg on 26.05.17.
 */
public class MyBean {
    private МyDependency mydep;

    public void execute() {
        mydep.bar();
        mydep.foo(100);
        mydep.foo(101);
    }

    public void setMydep(МyDependency dep) {
        mydep = dep;
    }
}

