package com.mycompany.app.aop_factory;

/**
 * Created by marg on 26.05.17.
 */
public class МyDependency {

    public void foo(int value) {
        System.out.println("foo() " + value);
    }

    public void bar() {
        System.out.println("bar()");
    }
}
