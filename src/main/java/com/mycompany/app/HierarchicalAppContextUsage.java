package com.mycompany.app;

import org.springframework.context.support.GenericXmlApplicationContext;

public class HierarchicalAppContextUsage {

    public static void main(String[] args) {
        GenericXmlApplicationContext parentCtx = new GenericXmlApplicationContext();
        parentCtx.load("META-INF/parent.xml");
        parentCtx. refresh () ;

        GenericXmlApplicationContext child = new GenericXmlApplicationContext();
        child.load("META-INF/app-context.xml");
        child.setParent(parentCtx);
        child. refresh () ;

        SimpleTarget targetl = (SimpleTarget) child.getBean("target1");
        SimpleTarget target2 = (SimpleTarget) child.getBean ("target2");
        SimpleTarget targetЗ = (SimpleTarget) child.getBean("target3");
        System.out.println(targetl.getVal());
        System.out.println(target2.getVal());
        System.out.println(targetЗ.getVal());

    }
}
